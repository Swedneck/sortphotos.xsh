#!/bin/env xonsh
import asyncio
import sys
import os
from datetime import datetime


def get_year(file):
	timestamp = os.path.getmtime(file)
	year = datetime.fromtimestamp(timestamp).year
	return year

def get_month(file):
	timestamp = os.path.getmtime(file)
	month = datetime.fromtimestamp(timestamp).month
	return f'{month:02}'

def move_file(file):
	year = get_year(file)
	month = get_month(file)
	mkdir -p @(target_dir_path)/@(year)/@(month)
	date = $(date +%r).strip()
	filename = $(basename @(file)).strip()
	print(f"[{date}] Moving {filename} to {target_dir}/{year}/{month}/")
	mv @(file) @(target_dir_path)/@(year)/@(month)/

async def main():
	for file in files:
		move_file(file)


if not 'ARG1' in ${...}:
	sys.exit("No source directory specified!")
source_dir_path = $(realpath $ARG1).strip()

if not 'ARG2' in ${...}:
	sys.exit("No target directory specified!")
target_dir_path = $(realpath $ARG2).strip()
target_dir = $ARG2

files = $(find @(source_dir_path) -type f).split('\n')[:-1:]

asyncio.run(main())

print("Sorted!")
